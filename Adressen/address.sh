#! /bin/bash

# address takes two arguments: a flag what to do as $1 and a handle to handle as $2
H=$2
F=$1
O=false
LOC=""
COL=""
BASEPATH=$(dirname $(realpath $0))

function usage {
	echo "Usage:
	./address.sh <option> <handle>
	Option is exactly one of the following:
	-h	Print this message und exit
        -l 	List all available handles and names.	
	With those two options, no <handle> ist needed.
	-g	Generate new entry
	-n	Print name
	-o	Print preferred Opening
	-c 	Print preferred Closing
	-a	Print address
	-f	Print fax number
	-t	Print telephone number
	-p	Print parent handle
	-e	Edit entry"
	exit 1
}

if [ "$F" = "-l" ] # -l nimmt kein weiteres Argument, im Gegensatz zu allen anderen Optionen.
then
	echo "handle: Name"
	while IFS="," read -r COL1 COL2 COLX
	do
	  echo "$COL1: $COL2"
	done < <(tail -n +2 $BASEPATH/Adressen.csv)
	exit 0
fi

if [ "$F" = "-h" ] # -h auch nicht, um genau zu sein.
then
	usage
fi

if [ $# -lt 2 ]
then 
	echo "Zu wenig Argumente
	"
	usage
fi

if [ $# -gt 2 ]
then 
	echo "Ignoriere überschüssige Argumente
	"
	usage
fi

case $F in
	"-n")
	COL=Name
	O=true
	;;

	"-o")
	COL=Anrede
	O=true
	;;

	"-a")
	COL=Adresse
	O=true
	;;

	"-f")
	# return fax number to be used by sipgate-cli
	COL=FaxNr
	O=true
	;;

	"-t")
	COL=TelNr
	O=true
	;;

	"-c")
	COL=Closing
	O=true
	;;

	"-p")
	COL=parent
	O=true
	;;
esac

if [ $O = true ]
then
	LOC=$(head -1 $BASEPATH/Adressen.csv | tr ',' '\n' | nl |grep -w "$COL" | tr -d " " | awk -F " " '{print $1}') #calculating position of requested Col
	grep "^$H," $BASEPATH/Adressen.csv | head -n 1 | cut -d "," -f$LOC | sed 's/"//g'
	#Returning requested column, making sure to return only one result (even though redundant results shouldn't be possible)
	exit 0
fi

if [ $F = "-e" ]
then
	NULL=$(grep "^$H," $BASEPATH/Adressen.csv)
	ISH=$? #exit code. Der if-Block wird ausgeführt, wenn nichts gefunden wurde. (ISH steht für is handle)
	if [ $ISH -eq 1 ]
	then
		read -p "Dieses Handle ist unbekannt. Neuen Datensatz anlegen? (j/n)" J
		if [ $J = "j"]
		then
			eval $0 -g $H
			exit 0
		else
			exit 1
		fi
	fi
	IFS=',' read -ra HEADER <<< $(grep "^handle," $BASEPATH/Adressen.csv) #read field keys
	IFS=',' read -ra VALUES <<< $(grep "^$H," $BASEPATH/Adressen.csv)     #read old values
	# for loop startet bei 1, um die handle Spalte nicht zu editieren:
	NEWLINE="$H"
	for (( i=1; i<${#HEADER[@]}; i++ ));
	do
		read -rp "${HEADER[$i]} (${VALUES[$i]}): " NEWVAL
#		echo "${HEADER[$i]} (${VALUES[$i]}): " 
		VALUES[$i]=$(echo ${NEWVAL:-${VALUES[$i]}} | sed -e s:,::g )
		NEWLINE="$NEWLINE,${VALUES[$i]}"  #already prepare output line
		if [ ${HEADER[$i]} = "parent" ]
		then 
			PARENT=${VALUES[$i]}
			NULL=$(grep "^$PARENT" $BASEPATH/Adressen.csv) # findet alles, wenn $PARENT leer ist.
			ISP=$? #exit code. Der if-Block wird ausgeführt, wenn nichts gefunden wurde.
			if [ $ISP -eq 1 ]
			then
				eval $0 -g $PARENT
			fi
		fi
	done
	echo "$(grep -v "^$H," $BASEPATH/Adressen.csv)" > $BASEPATH/Adressen.csv
	echo $NEWLINE >> $BASEPATH/Adressen.csv
fi

if [ $F = "-g" ]
then
	NULL=$(grep "^$H," $BASEPATH/Adressen.csv)
	ISH=$? #exit code. Der if-Block wird ausgeführt, wenn etwas gefunden wurde. (ISH steht für is handle)
	if [ $ISH -ne 1 ]
	then
		echo "Handle schon vergeben"
		exit 1
	fi
	echo "==========Generate Address for handle $H=================="
	read -p 'Name: ' NAME
	read -p 'Anrede (default: "Damen und Herren"): ' ANREDE
	read -p 'Straße und Hausnummer: ' STREET
	read -p 'PLZ und Ort: ' CITY
	read -p 'Faxnummer: ' FAX
	read -p 'Telefonnummer: ' TEL
	read -p 'Schlussformel (default: "Mit der Ihnen gebührenden Hochachtung"):' CLOSING
	read -p 'Nächsthöhere Hierarchieebene (handle): ' PARENT
	ANREDE=${ANREDE:-"Damen und Herren"}
	CLOSING=${CLOSING:-"Mit der Ihnen gebührenden Hochachtung"}
	echo "$H,$NAME,$ANREDE,$STREET\\\\$CITY,$FAX,$TEL,$CLOSING,$PARENT" >> $BASEPATH/Adressen.csv
	NULL=$(grep "^$PARENT" $BASEPATH/Adressen.csv) # findet alles, wenn $PARENT leer ist.
	ISP=$? #exit code. Der if-Block wird ausgeführt, wenn nichts gefunden wurde.
	if [ $ISP -eq 1 ]
	then
		eval $0 -g $PARENT
	fi
fi
