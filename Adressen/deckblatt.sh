#! /bin/bash

EDIT=false
STA=
GEGSAVE=
NEW=
BASEPATH=$(dirname $(realpath $0))

if [ -n $1 ] && [ "$1" = "edit" ]
then
	EDIT=true
fi

if [ -e DECKBLATT.csv ]
then
	# Formatiert anzeigen
	echo "Deckblatt für $PWD"
	join -t, -1 2 -2 1 <(sort -t, -k2 DECKBLATT.csv) <(sort $BASEPATH/Adressen.csv) -o 1.4,2.2,1.2,1.3 -a 1 --nocheck-order | grep -v handle | sort | awk -F, '{ print $2 " (" $3 "): " $4 }' | sed -e 's|"||g' -e 's|^ ()|Mein Az|'
else
	# Neu anlegen
	read -p "Kein Deckblatt im Verzeichnis. Lege neues Deckblatt an? " NEW
	if [[ ! $NEW =~ [yYjJ] ]]; then exit 0; fi
	read -p "Mein Aktenzeichen: " MYAZ
	echo "aktuelle Gegenseite?"
	select GEG in AG LG OLG SG VG OVG BVG BVerfG Pol StA JC
       	do if [ ! -z "$GEG" ]; then break; fi; done
	echo Auswahl: $GEG
	read -p "Deren Aktenzeichen: " AZ
	until [ $GEGHANDLE ]
	do
		read -p "Das eindeutige Kürzel für die Gegenseite: " GEGHANDLE
		GEGNAME=$($BASEPATH/address.sh -n $GEGHANDLE)
		if [ -z "$GEGNAME" ]
		then
			read -p "$GEGHANDLE nicht gefunden. Neu anlegen? (y/n) " GEGSAVE
			if [[ $GEGSAVE =~ [yYjJ] ]]; then $BASEPATH/address.sh -g $GEGHANDLE ; else GEGSAVE= ; fi
		else
			read -p "$GEGNAME als Gegenseite bestätigen? (y/n) " GEGSAVE
			if [[ ! $GEGSAVE =~ [yYjJ] ]]; then GEGSAVE= ; fi
		fi
	done
	if [[ $GEG =~ AG|LG|OLG|BGH ]]
	then
		read -p "Gibt es ein Aktenzeichen der StA? Falls ja, bitte eingeben. Falls nein, einfach Enter drücken: " STAZ
	fi
	if [ "$STAZ" ]
	then
		STA="StA,,$STAZ,\n"
	fi
	echo -e "INH,handle,az,stand\nME,,$MYAZ,0\n${STA}$GEG,$GEGHANDLE,$AZ,+" > DECKBLATT.csv
fi
 
if [ $EDIT = true ]
then
	echo "Was möchtest du tun?"
	OPT1="Vorhandene Instanz zuständig machen"
	OPT2="Neues Aktenzeichen anlegen"
	select ACTION in "$OPT1" "$OPT2" 
	do if [ -n "$ACTION" ]; then break; fi; done
	case "$ACTION" in 
		"$OPT1")
			sed -i.bac -e "s/\+$//" DECKBLATT.csv
			select NEWZ in $(cut -d, -f2 DECKBLATT.csv | tail -2) #Titelzeile und eigene Zeile weglassen
			do if [ ! -z "$NEWZ" ]; then break; fi; done
			LN=$(nl DECKBLATT.csv | grep -w $NEWZ | cut -f1 | tr -d " ")
			sed -i -e "${LN}s/$/+/" DECKBLATT.csv
			;;
		       
		"$OPT2")
			# Prüfen, ob parent handel existiert.
			CURRENT=$(grep "+" DECKBLATT.csv | cut -d, -f2)
			PARENT=$($BASEPATH/address.sh -p "$CURRENT")
			echo "CURRENT=$CURRENT, PARENT=$PARENT"
			USE= #for scope
			# Dieses vorschlagen, oder manueller Eintrag
			if [[ -n $PARENT && -n "$($BASEPATH/address.sh -n "$PARENT")" ]]
			then
				read -p "Nächste Instanz sollte "$($BASEPATH/address.sh -n "$PARENT")" ($PARENT) sein. Verwenden? (j/n): " USE
				if [[ $USE =~ [nN] ]]
			       	then 
					read -p "Neue Instanz eingeben (handle): " PARENT
				fi
				read -p "Aktenzeichen der neuen Instanz: " AZ
				GEG=$(echo $PARENT | egrep -o ^[[:upper:]]* )
				case $GEG in
					BV) GEG=BVerfG ;;
					POL) GEG=Pol ;;
					STA) GEG=StA ;;
				esac
				if [[ ! $GEG =~ AG|LG|OLG|SG|VG|OVG|BVG|BVerfG|Pol|StA|JC ]]
				then
					echo "Konnte Instanzart nicht raten, bitte manuell eingeben:"
					select GEG in AG LG OLG SG VG OVG BVG BVerfG Pol StA JC
					do if [ ! -z "$GEG" ]; then break; fi; done
				fi
			fi
			# Im Zweifel mit address.sh -g
			if [[ -n $PARENT && ! -n "$($BASEPATH/address.sh -n "$PARENT")" ]]
			then
				echo "Noch kein Adressbucheintrag für '$PARENT' vorhanden"
				$BASEPATH/address.sh -g $PARENT 
				[ $? = 0 ] || exit 1
			fi
			if [ -n $PARENT ]
			then
				sed -i.bac -e "s/\+$//" DECKBLATT.csv
				echo "$GEG,$PARENT,$AZ,+" >> DECKBLATT.csv
			fi
			;;	
	esac
fi
