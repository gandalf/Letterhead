# Letterhead

Letterhead sammelt Vorlagen und eine Adressverwaltung für mein privates Aktenverwaltungssystem.

## LaTeX
Mein neuer Briefkopf arbeitet mit der Komascript-Klasse `scrlttr2`. 

Die Dateigestaltung ist noch chaotisch und superprovisorisch

### Schnittstellen
Einige Schnittstellen verwenden -offen oder verdeckt- Komavariablen:
`yourref` für das **Aktenzeichen**. Dieses wird als per `setkomavar{yourref}` gesetzt, kann aber als `\az` ausgelesen werden.
Anders das **Briefdatum**, das nur als Komavariable `yourmail` angesprochen wird.
Der `subject`-Befehl aus `letter` wurde als Wrapper um `setkomavar{subject}` reimplementiert.

Die **automatische Adresswahl** funktioniert mit dem Kommando `\receive`:
`\receive*{<handle>}`
: `<handle>` ist ein Primärschlüssel aus der Datenbank in `Adressen.csv`, deren Pfad aktuell noch hardcoded ist.

`\receive{<handle>}`
: in dieser Version ist `<handle>` ein Funktionshandle `INH` aus der Datei `DECKBLATT.csv`

`\receive`
: ohne jegliche Argumente holt sich `\receive` die Adresse des aktuellen Hauptbrieffeindes aus der Datei `DECKBLATT.csv` (stand=="+")

### Deployment
Zur Nutzung von `\receive` muss in der `.lco` oder im Brief der Pfad zu Letterhead definiert werden durch 
```
\renewcommand\addressbookpath{\your\path\to\Letterhead}
```
Ebenso muss für die Option `subscript` eine Bilddatei mit der Unterschrift spezifiziert werden durch 
```
\renewcommand\autogramm{\path\to\file}
```

### Templates
`\einspruch{<Art>}`
: `<Art>` sollte `Strafbefehl` oder `Bußgeldbescheid` sein. Aktenzeichen und Briefdatum sollten über die Schnittstellen, am Besten über `\receive` vorher gesetzt sein, da dieser Befehl diese Werte per Schnittstelle abruft.

`\rechtsmittel[<Art>]{<Datum>}{protokoll}`
: `<Art>` defaults to `zunächst unbestimmtes Rechtsmittel`. Sonst hier `Revision`,  `Berufung` oder `Rechtsbeschwerde` eintragen. `protokoll` ist einzutragen, wenn gleichzeitig die Zusendung des Protokolls beantragt werden soll, ansonsten leere Klammern lassen.

## Adressdatenbank
Angedacht ist folgendes Konzept:

Eine zentrale Adressdatenbank:
```
Adressen.csv
============
handle,Name,Anrede,Adresse,FaxNr,TelNr,Closing,parent
```
Wo `handle` und `parent` datenbankinterne Schlüssel sind, `parent` enthält das `handle` der nächsthöheren Hierarchieebene: Bei Personen die zugehörige Behörde/Organisation, bei Gerichten das zuständige Rechtsmittelgericht.


In jedem Aktenordner zwei Dateien
```
DECKBLATT.csv
=============
INH,handle,aktenzeichen,stand
```
Wo `INH ∈ {AG,LG,StA,OLG,BVerfG,VG,OVG,GEG,SG}` und `stand='+'` in der Zeile des aktuellen Hauptbrieffeindes.


### Skripte
#### `address.sh`
Führt durch die Eintragung in die Adressdatenbank oder gibt einzelne Werte aus selbiger zurück. (Zur Verwendung durch `sipgate-cli` zB)
```
Usage:
        ./address.sh <option> <handle>
        Option is exactly one of the following:
        -h      Print this message und exit
        -l      List all available handles and names.
        With those two options, no <handle> ist needed.
        -g      Generate new entry
        -n      Print name
        -o      Print preferred Opening
        -c      Print preferred Closing
        -a      Print address
        -f      Print fax number
        -t      Print telephone number
        -p      Print parent handle
        -e      Edit entry
```

Wird `<handle>` nicht angegeben, soll nach der aktuell zuständigen Stelle in einer `DECKBLATT.csv` gesucht werden. Das ist aber Zukunftsmusik, die Angabe von `<handle>` wird, außer bei `-l` und `-h`, immer verlangt.
#### `deckblatt.sh`
Führt durch die Anlage eines Deckblattes ~~und stellt sicher, dass die korrekte Gerichtshierarchie eingetragen ist~~.
Falls schon ein Deckblatt existiert, soll dieses angezeigt werden.
Außerdem soll damit ein existierendes Deckblatt editiert werden können: Zeilen hinzufügen oder Zuständigkeit verschieben.
#### `rechtsmittel.sh`
Tut, in einer Akte aufgerufen, zwei Dinge:
1. ein Fax unter Verwendung der LaTeX-Templates `\einspruch` bzw. `\rechtsmittel` anlegen, 
    * zur Kontrolle anzeigen und 
    * wegfaxen
2. das `DECKBLATT.csv` aktualisieren. Wenn nötig (per `address.sh`?) die Daten des Rechtsmittelgerichts erfragen.
