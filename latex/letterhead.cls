%% 
%% This is file `letterhead.cls',
%% by Gandalf 
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{letterhead}[2021/03/15 scrlttr2 derived letter class for automatisation of legal letters]

\RequirePackage{datatool}
\RequirePackage{etoolbox}
\RequirePackage{graphicx}
\newcommand\subscript{}
\newcommand\autogramm{}
\newcommand\addressbookpath{}

\DeclareOption{subscript}{
	\renewcommand{\subscript}{
		\@setplength{sigbeforevskip}{0cm}
		\setkomavar{signature}{\includegraphics[height=1.4cm]{\autogramm}\\\usekomavar{fromname}}
	}
}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrlttr2}}
\ProcessOptions\relax

\LoadClass{scrlttr2}

\setkomavar*{yourref}{Aktenzeichen}
\setkomavar*{myref}{Mein Zeichen}
\newcommand{\subject}[1]{\setkomavar{subject}{#1}}
\newcommand{\az}{\usekomavar{yourref}}
\newcommand{\eilt}{\setkomavar{specialmail}{Eilt!}}
\newcommand{\Eilt}{\setkomavar{specialmail}{Eilt sehr!}}
\newcommand{\Haftsache}{\setkomavar{specialmail}{Eilt sehr! Haftsache!}}
\subscript


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Adress detection
% usage: \receive      % aktueller Hauptbrieffeind
%        \receive{INH} % INH aus DECKBLATT.csv
%        \receive*{handle} % handle aus Adressen.csv
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand*{\receivestar}[1]{
	\DTLloaddb{adressen}{\addressbookpath/Letterhead/Adressen/Adressen.csv}
	\DTLassignfirstmatch{adressen}{handle}{#1}{\Name=Name,\Adresse=Adresse,\FaxNr=FaxNr,\TelNr=TelNr,\Opening=Anrede,\Closing=Closing}
	\newcommand\FullAddress{\Name\\\Adresse\\per Fax: \FaxNr}
	\ifstrequal{\Opening}{}{\renewcommand\Opening{Damen und Herren}}{}
	\ifstrequal{\Closing}{}{\renewcommand\Closing{Mit der Ihnen gebührenden Hochachtung}}{}
}

\newcommand*{\receivenostar}[1][+]{
	\DTLloaddb{deckblatt}{DECKBLATT.csv}
	\ifstrequal{#1}{+}{%
		\DTLassignfirstmatch{deckblatt}{stand}{+}{\INH=INH,\handle=handle,\aktenzeichen=az}%
	}{%
		\DTLassignfirstmatch{deckblatt}{INH}{#1}{\INH=INH,\handle=handle,\aktenzeichen=az}%
	}
	\DTLassignfirstmatch{deckblatt}{INH}{ME}{\myref=az}
	\DTLloaddb{adressen}{/home/bernhardt/Entwicklung/Letterhead/Adressen/Adressen.csv}
	\xDTLassignfirstmatch{adressen}{handle}{\handle}{\Name=Name,\Adresse=Adresse,\FaxNr=FaxNr,\TelNr=TelNr,\Opening=Anrede,\Closing=Closing}
	\setkomavar{yourref}{\aktenzeichen}
	\setkomavar{myref}{\myref}
	\newcommand\FullAddress{\Name\\\Adresse\\per Fax: \FaxNr}
	\ifstrequal{\aktenzeichen}{}{\ifstrequal{\INH}{VG}{\setkomavar{yourref}{Neue Klage}}{}}{}
	\ifstrequal{\Opening}{}{\renewcommand\Opening{Damen und Herren}}{}
	\ifstrequal{\Closing}{}{\renewcommand\Closing{Mit der Ihnen gebührenden Hochachtung}}{}
	\ifstrequal{#1}{JC}{\addtoreffields{bgnr}}{}
}
\newcommand*{\receive}{\@ifstar{\receivestar}{\receivenostar}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Templates
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\einspruch}[1]{Hiermit lege ich Einspruch gegen den #1 Az. \az\ vom \usekomavar{yourmail} ein.

Ich beantrage zum Zwecke der Akteneinsicht nach §\,149\,Abs.\,7\,StPO die Zusendung der vollständigen Verfahrensakte bzw. Kopien davon an meine Adresse.}

\newcommand{\rechtsmittel}[3][zunächst unbestimmtes Rechtsmittel]{Hiermit lege ich #1 gegen das Urteil vom #2 unter dem Aktenzeichen \az\ ein.

\ifstrequal{#3}{protokoll}{Ich beantrage zugleich die Zusendung des vollständigen Protokolls der Hauptverhandlung an meine Adresse.}\
}
